# FoundryVTT Status Icon Counters

This is the repository of the status icon counter addon for FoundryVTT.

## Usage

**Status Icon Counters** allows setting and displaying a counter on any token's status effects.

![Status Icon Counters](sample.png "Status Icon Counters")

The module has several options. The **font size** and **color** should be self-explanatory.

The **mouse rebind** option changes the click behavior as follows:
- Left click on a status icon (in the token HUD) increases the stack count by 1. If it wasn't active before, it is added.
- Right click on a status icon decreases the stack count by 1. If it reaches 0, the effect is removed. If it is already inactive, the overlay effect is toggled instead.
- Shift + left click on a status icon always toggles the overlay effect.

The **number key rebind** behaves as follows:
- Pressing any number key while hovering an effect sets the stack count to that number. If it wasn't active before, it is added.
- Pressing 0 removes the effect.

When the mouse button rebinds are disabled, the stack count is reset each time the effect is toggled.

----

In addition to the standard features, this module allows using a counter as an active effect duration. Any status implemented using the active effect format (available from FoundryVTT 0.7.4 onward) can change its type using **Ctrl + Left click**:
- The **round countdown** is automatically decreased by 1 each round.
- The **turn countdown** is decreased by 1 each turn. Note that it may display odd values when going back into the previous round.
- The **multiplier** type applies its stack count to any underlying active effect changes (e.g. if an effect would decrease your health by 4, its value will be increased to 8 for 2 stacks, 12 for 3 stacks, etc.). For effects that are already multipliers, the change is modified additively (e.g. 0.8 for 1 stack becomes 0.6 for 2 stacks, not 0.64).

System developers can define their own counter types and defaults for each effect. Read the documentation below to find out how.

## Development

Everything you (should) need is contained in the [counter API](API.md). If it's not, feel free to open a feature suggestion on this repository.

The `EffectCounter` class provides several static methods and is added to the window as an optional dependency:

```javascript
if (!window.EffectCounter) {
    console.warn("Counter API not (yet?) loaded.");
}
```

Alternatively, your module can define a dependency in its module.json and `include ../../modules/statuscounter/module/api.js`.

Most instance methods take an optional `parent` parameter that you can use to avoid unnecessary lookups, but you don't have to. If left empty, the module will resolve it using the token ID and icon path.

Here are some common examples of things you might want to do with the status counters:

### Fetch a counter

The static `findCounter` methods can be used to get any counter of the active scene using its token and icon path. This works for simple token effects as well as active effects.

```javascript
let token = new Token(); // Pretend that this isn't empty
let simpleValue = EffectCounter.findCounter(token, token.effects[0]).getDisplayValue();
let activeValue = EffectCounter.findCounterValue(token, token.actor.effects[0].icon);
```

If you need more than one, use `getAllCounters` and apply your own logic:
```javascript
let activeEffectCounters = ActiveEffectCounter.getCounters(token);
let simpleTokenCounters = EffectCounter.getCounters(token);
let both = EffectCounter.getAllCounters(token);
let effectIconPaths = both.map(counter => counter.path);
```

In both cases, the result is fully typed, so you may use instance methods or use `instanceof` to determine the effect type.

### Modify a counter

Modifications are handled internally so you don't have to worry about whether the effect is a simple token effect or an active effect. When calling `setValue`, an update is generated automatically (unless a custom type prevented it).

```javascript
let counter = EffectCounter.findCounter(token, iconPath);
await counter.setValue(counter.getValue() * 2);
```
For built-in types, setting a value equal or below 0 will remove the effect from the token. Custom types may changes this behavior to allow negative values.

Creating a counter with `new ActiveEffectCounter(1, "icon/path.svg", token)` does not automatically send an update to allow prior modifications. Instead, you need to call `counter.update()` manually. This method can also be used to force an update regardless of value changes. To remove an effect and its counter, call `counter.remove()`.

### Listen for counter changes

You can hook onto `updateToken` and `updateActiveEffect` in order to receive events for counter changes. Sadly, this needs to be done separately for simple and active effects. Also keep in mind that the data contents aren't typed, although you may create typed counters using them. Example:

```javascript
Hooks.on("updateToken", function(scene, tokenData, diffData, options, userId) {
    // Case 1: Simple effect on the token
    let effectCounters = foundry.utils.getProperty(diffData, "flags.statuscounter.effectCounters"));

    if (hasProperty(diffData, "actorData.effects")) {
        // Case 2: Active effect for non-linked tokens
        effectCounters = diffData.actorData.effects.map(effectData => effectData.flags.statuscounter.counter);
        let typedEffectCounters = effectCounters.map(counterData => new ActiveEffectCounter(counterData));
        // Or: let typedEffectCounters = ActiveEffectCounter.getCounters(canvas.tokens.get(tokenData._id));
    }

    if (effectCounters) { /* Do something */ }
});

Hooks.on("updateActiveEffect", function(actor, effectData, diffData, options, userId) {
    // Case 3: Active effect for token with linked actor
    let effectCounter = foundry.utils.getProperty(diffData, "flags.statuscounter.counter");
    if (effectCounter) { /* Do something */ }
});
```

### Add your own counter type

The API also allows you to create custom counter types that can have their own logic and font. A self-contained example can be found in the [countdown implementation](module/countdown.js).

To add a type, use:

```javascript
CounterTypes.addType("mymodule.thechosenone",
    function(counter, parent) { return 1; }, // Value getter
    function(counter, value, parent) { // Value setter
        if (value === 1) {
            counter.value = 1;
            counter.visible = true;
            return true; // Update the counter
            // return false // Or don't
        }

        console.error('not the chosen one');
        return null; // Remove the counter
    },
    function(counter, type, parent) { return true; }); // Type allowed for the counter
```

All three functions can be nulled and will then use defaults (which will then access the value of the counter, write the value of the counter or always be visible). Note that your setter is also responsible for updating the counter's `visible` property (true or false), indicating whether the number is rendered on the effect icon.

After registering a type, you can change its font...

```javascript
let font = CONFIG.canvasTextStyle.clone();
font.fontSize = game.settings.get("statuscounter", "counterFontSize");
CounterTypes.setFont("mymodule.thechosenone", font);
```

... or assign effects that should use that type by default:

```javascript
CounterTypes.setDefaultType("icons/chosen1.svg", "mymodule.thechosenone");
```

The default type can also be configured globally (for effects that have no explicit default) using the `statuscounter.defaultType` setting. Note that this is registered in the *setup* hook, so any overrides should be done in the *ready* hook.
